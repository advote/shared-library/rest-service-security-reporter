/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.security;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.github.javaparser.ast.expr.SingleMemberAnnotationExpr;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

@Mojo(name = "generate-security-report",
      requiresDependencyResolution = ResolutionScope.COMPILE,
      defaultPhase = LifecyclePhase.PACKAGE)
public class SecurityReport extends AbstractMojo {


  @Parameter(readonly = true, required = true, property = "project", defaultValue = "${project}")
  private MavenProject project = null;


  public void execute()
      throws MojoExecutionException {

    getLog().warn("This plugin has been created for a specific context");
    getLog().warn("In particular it may omit to retrieve some roles");
    File sourceDirectory = new File(project.getBuild().getSourceDirectory());
    File target = new File(new File(project.getBuild().getOutputDirectory()).getParentFile().toString()
                           + File.separatorChar + "security-report.csv");

    try (Stream<Path> paths = Files.walk(sourceDirectory.toPath());
         FileOutputStream targetOutputStream = new FileOutputStream(target)) {
      List<ControllerVisitsResults.ControllerVisitResult> partialReports = createPartialReport(paths);
      controlAllServiceHasAtLeastOneRoleRequired(partialReports);
      targetOutputStream.write(generateReport(partialReports).getBytes(Charset.forName("UTF-8")));
      getLog().info("Report has been generated : " + target);
    } catch (IOException e) {
      throw new MojoExecutionException(e.getMessage(), e);
    }

  }

  private List<ControllerVisitsResults.ControllerVisitResult> createPartialReport(Stream<Path> paths) {
    return paths
        .map(Path::toFile)
        .filter(File::isFile)
        .filter(file -> file.getName().endsWith(".java"))
        .map(this::toPartialReport)
        .flatMap(report -> report.controllerVisitResults.stream())
        .collect(Collectors.toList());
  }

  private String generateReport(List<ControllerVisitsResults.ControllerVisitResult> partialReports) {
    List<String> allApplicationRoles = partialReports
        .stream()
        .flatMap(r -> r.roles.stream())
        .sorted()
        .distinct()
        .collect(Collectors.toList());

    String header = Stream.concat(Stream.of("Class", "path", "method"), allApplicationRoles.stream())
                          .collect(Collectors.joining(";"));


    String content = partialReports
        .stream()
        .map(report ->
                 Stream.concat(Stream.of(report.className, report.path, report.method),
                               allApplicationRoles
                                   .stream()
                                   .map(role -> report.roles.contains(role) ? "X" : ""))
                       .collect(Collectors.joining(";"))
        ).collect(Collectors.joining("\n"));

    return header + "\n" + content;
  }

  private void controlAllServiceHasAtLeastOneRoleRequired(List<ControllerVisitsResults.ControllerVisitResult>
                                                              partialReports)
      throws MojoExecutionException {
    List<ControllerVisitsResults.ControllerVisitResult> serviceWithNoRights =
        partialReports.stream().filter(pr -> pr.roles.isEmpty()).collect(Collectors.toList());

    serviceWithNoRights.forEach(
        pr -> getLog()
            .error(
                String.format(
                    "service %s does not contains any role if needed add @SuppressWarnings(\"anonymous_user\")", pr))
    );

    if (!serviceWithNoRights.isEmpty()) {
      throw new MojoExecutionException("Some services has no rights");

    }
  }

  private ControllerVisitsResults toPartialReport(File file) {
    try {
      FileInputStream in = new FileInputStream(file);
      CompilationUnit cu = JavaParser.parse(in);
      ControllerVisitsResults visitResult = new ControllerVisitsResults();
      cu.accept(new ControllerVisitor(), visitResult);
      return visitResult;
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    }
  }

  private class ControllerVisitor extends VoidVisitorAdapter<ControllerVisitsResults> {

    @Override
    public void visit(ClassOrInterfaceDeclaration n, ControllerVisitsResults visitResult) {
      if (n.getAnnotationByName("RestController").isPresent()) {
        visitResult.addNewController(n.getName().asString());
        n.getAnnotationByName("RequestMapping").ifPresent(
            annotationExpr -> visitResult.current().setBaseRequestMapping(getRequestMapping(annotationExpr))
        );

        n.getAnnotationByName("PreAuthorize").ifPresent(
            annotationExpr -> visitResult.current().setBaseRoles(getRoles(annotationExpr))
        );

      }
      super.visit(n, visitResult);
    }


    @Override
    public void visit(MethodDeclaration n, ControllerVisitsResults visitResult) {
      boolean requireRole = n.getAnnotationByName("SuppressWarnings").map(
          annotationExpr -> !annotationExpr.toString().toLowerCase(Locale.ENGLISH).contains("anonymous_user")
      ).orElse(true);

      if (!requireRole) {
        return;
      }


      n.getAnnotationByName("RequestMapping").ifPresent(
          annotationExpr -> visitResult.current().setMethodRequestMapping(getRequestMapping(annotationExpr))
      );

      n.getAnnotationByName("PreAuthorize").ifPresent(
          annotationExpr -> visitResult.current().setMethodRoles(getRoles(annotationExpr))
      );


      visitResult.flush();
      super.visit(n, visitResult);
    }


    private Set<String> getRoles(AnnotationExpr a) {
      if (a instanceof SingleMemberAnnotationExpr) {
        return Stream.of(((SingleMemberAnnotationExpr) a)
                             .getMemberValue().toString()
                             .replaceAll(".*\\((.*)\\)", "$1")
                             .replaceAll("(\\s|\"|\')", "")
                             .split(","))
                     .collect(Collectors.toSet());
      }
      return Collections.emptySet();
    }

    private RequestMapping getRequestMapping(AnnotationExpr a) {
      if (a instanceof NormalAnnotationExpr) {
        NormalAnnotationExpr annotation = (NormalAnnotationExpr) a;
        String path = annotation.getPairs().stream()
                                .filter(m -> "value".equals(m.getName().asString()))
                                .findFirst()
                                .map(m -> m.getValue().toString())
                                .orElse(null);

        String method = annotation.getPairs().stream()
                                  .filter(m -> "method".equals(m.getName().asString()))
                                  .findFirst()
                                  .map(m -> m.getValue().toString().replaceAll("./\\.", ""))
                                  .orElse(null);

        return new RequestMapping(path, method);
      } else if (a instanceof SingleMemberAnnotationExpr) {
        return new RequestMapping(((SingleMemberAnnotationExpr) a).getMemberValue().toString());
      }
      return null;
    }
  }
}
